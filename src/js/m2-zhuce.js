import interfaceApi from '../api/urlListData.js';
import validatorUtils from '../common/validatorUtils.js';
import ajax from '../common/ajax.js'


$('.sen-code').click(function () {
    if($(".tel-phone").val() == '' || $(".tel-phone").val() == null){
        alert('不能为空')
        return
    }else if(!(validatorUtils.REGEX_RULE.mobilephone.test($(".tel-phone").val()))){
        alert('手机号码不对啊啊啊啊啊')
    }else {
        let countdown = 60;
        sendEmail();
        function sendEmail() {
            let dom = $(".sen-code");
            setTime(dom);
            sendAuthenticationCode();//调用发送验证码接口
        }
        function setTime(dom) { //发送验证码倒计时
            if (countdown == 0) {
                dom.removeClass('add-den-code');
                dom.attr('disabled', false);
                dom.val("发送验证码");
                countdown = 60;
                return;
            } else {
                dom.attr('disabled', true);
                dom.addClass('add-den-code');
                dom.val("重新发送(" + countdown + ")");
                countdown--;
            }
            setTimeout(function () {
                setTime(dom)
            }, 1000)
        }
    }
});
//发送验证码接口
function sendAuthenticationCode() {
    let tel = $(".tel-phone").val();
    let postData = {
        tel:tel
    };
    ajax({
        url: interfaceApi.Api.smsCode,
        postData:postData,
        type:'POST',
        dataType:'json',
        succCallback:function(res){
            console.log(res)
        },
        errorCallback:function (res) {
            console.log(res)
        }
    });
}

